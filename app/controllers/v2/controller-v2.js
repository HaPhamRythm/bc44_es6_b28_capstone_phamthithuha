export let renderFoodList = (foodArr) => {
  let contentHTLM = "";
  foodArr.forEach((item) => {
    let { foodID, tenMon, loai, giaMon, khuyenMai, tinhTrang } = item;
    console.log(item);
    let contentTr = `<tr>
        <td>${foodID}</td>
        <td>${tenMon}</td>
        <td>${loai ? "Chay" : "Mặn"}</td>
        <td>${giaMon}</td>
        <td>${khuyenMai}</td>
        <td>${item.giaKM()}</td>
        <td>${tinhTrang ? "Còn" : "Hết"}</td>
        <td>
        <button class="btn btn-danger" onclick="deleteID(${foodID})">Delete</button>
        <button data-toggle="modal"
        data-target="#exampleModal" class="btn btn-success" onclick="editID(${foodID})">Edit</button></td>
        </tr>`;
    contentHTLM += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTLM;
};

export function showInfoToEdit(food) {
  document.getElementById("foodID").innerText = food.foodID;
  document.getElementById("tenMon").innerText = food.tenMon;
  document.getElementById("loai").innerText =
    food.loai == "loai1" ? "Chay" : "Mặn";
  document.getElementById("giaMon").innerText = food.giaMon;
  document.getElementById("khuyenMai").innerText = food.khuyenMai;

  document.getElementById("tinhTrang").innerText = food.tinhTrang;
  document.getElementById("hinhMon").src = food.hinhMon;
  document.getElementById("moTa").innerText = food.moTa;
}
