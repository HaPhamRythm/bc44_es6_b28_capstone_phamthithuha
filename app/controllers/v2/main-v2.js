import { renderFoodList, showInfoToEdit } from "./controller-v2.js";
import { Food } from "../../models/v1/model.js";
import { getInfoFromForm } from "../v1/controller-v1.js";
// fetch = get info from mockapi and render
const BASE_URL = `https://64570c475f9a4f236150c129.mockapi.io/food`;
let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let newObjectWithGiaKM = res.data.map((item) => {
        let { name, type, discount, img, desc, price, status, id } = item;
        let food = new Food(id, name, type, price, discount, status, img, desc);
        return food;
      });
      renderFoodList(newObjectWithGiaKM);
      console.log(newObjectWithGiaKM);
    })
    .catch((err) => {
      console.log(err);
    });
};

fetchFoodList();

// delete

function deleteID(id) {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchFoodList(res.data);
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}

window.deleteID = deleteID;

// add

function add() {
  let newFood = getInfoFromForm();

  let newFoodToAPI = {
    name: newFood.tenMon,
    type: newFood.loai,
    discount: newFood.khuyenMai,
    img: newFood.hinhMon,
    desc: newFood.moTa,
    price: newFood.giaMon,
    status: newFood.tinhTrang,
    id: newFood.foodID,
  };

  axios({
    url: `${BASE_URL}`,
    method: "POST",
    data: newFoodToAPI,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList(res.data);
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}

window.add = add;

// edit

function editID(id) {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      let { id, type, price, img, status, desc, discount, name } = res.data;
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;

      document.getElementById("tinhTrang").value = status ? "1" : "0";
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = desc;
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}

window.editID = editID;

function updateItem() {
  let editedFood = getInfoFromForm();
  let editedFoodToAPI = {
    name: editedFood.tenMon,
    type: editedFood.loai == "loai1" ? true : false,
    discount: editedFood.khuyenMai,
    img: editedFood.hinhMon,
    desc: editedFood.moTa,
    price: editedFood.giaMon,
    status: !!(editedFood.tinhTrang * 1),
    id: editedFood.foodID,
  };
  axios({
    url: `${BASE_URL}/${editedFood.foodID}`,
    method: "PUT",
    data: editedFoodToAPI,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
}
window.updateItem = updateItem;
