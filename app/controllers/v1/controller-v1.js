export function getInfoFromForm() {
  let foodID = document.getElementById("foodID").value;
  let tenMon = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value * 1;
  let khuyenMai = document.getElementById("khuyenMai").value * 1;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;
  return {
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
}

export function showInfoToUser(food) {
  document.getElementById("spMa").innerText = food.foodID;
  document.getElementById("spTenMon").innerText = food.tenMon;
  document.getElementById("spLoaiMon").innerText =
    food.loai == "loai1" ? "Chay" : "Mặn";
  document.getElementById("spGia").innerText = food.giaMon;
  document.getElementById("spKM").innerText = food.khuyenMai;
  document.getElementById("spGiaKM").innerText = food.giaKM();
  document.getElementById("spTT").innerText = food.tinhTrang;
  document.getElementById("imgMonAn").src = food.hinhMon;
  document.getElementById("pMoTa").innerText = food.moTa;
}
