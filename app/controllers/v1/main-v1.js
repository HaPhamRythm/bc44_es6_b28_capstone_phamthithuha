// get info from form and render

import { getInfoFromForm } from "./controller-v1.js";
import { Food } from "../../models/v1/model.js";
import { showInfoToUser } from "./controller-v1.js";
function themMon() {
  let data = getInfoFromForm();
  let { foodID, tenMon, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } =
    data;
  let food = new Food(
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  showInfoToUser(food);
  console.log(food);
}

window.themMon = themMon;
